# Vegoa Website

You can visit the official website of Vegoa at [vegoa.vegoans.org](https://vegoa.vegoans.org).

## Content Generator

The website uses the Jekyll Static Code Generator and is currently based on the Clean Blog theme by [Start Bootstrap](http://startbootstrap.com/).

If you have Jekyll installed, simply run `jekyll serve` in your command line and preview the build in your browser. You can use `jekyll serve --watch` to watch for changes in the source files as well.

A Grunt environment is also included. There are a number of tasks it performs like minification of the JavaScript, compiling of the LESS files, adding banners to keep the Apache 2.0 license intact, and watching for changes. Run the grunt default task by entering `grunt` into your command line which will build the files. You can use `grunt watch` if you are working on the JavaScript or the LESS.

You can run `jekyll serve --watch` and `grunt watch` at the same time to watch for changes and then build them all at once.

## Contributing

Please consider reading the [contributing guide](CONTRIBUTING.md) if you want to contribute to the project or create new content.

## Code of Conduct

The community is one of the best features of Vegoa, and we want to ensure it remains welcoming and safe for everyone.
We have adopted the Contributor Covenant for all projects in the @Vegoa Gitlab group, the discussion forum, chat rooms, mailing list, social media tools, meetups and any other public event related to Vegoa.
This code of conduct outlines the expectations for all community members, as well as steps to report unacceptable behavior.
We are committed to providing a welcoming and inspiring community for all and expect our code of conduct to be honored.

* **The Code of Conduct is available [here](CODE_OF_CONDUCT.md).**
