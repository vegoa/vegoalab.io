---
layout: post
title:  "Make your own oat milk!"
date:   2016-04-13 21:36:12 +0100
categories: degrowth
author: "Simona Vinati"
---

I won’t go into details about the reasons why I don’t buy plant based milks in stores, instead I am sharing with you one of the easiest recipe to prepare your own oat milk! Enjoy =)

![](/img/oatmilk1.jpg)

OAT MILK
I am not adding sweeteners because I use it also for savoury dishes (creamy dishes, sauces such as besciamelle)

Ingredients:

* 1 lt of water

* 80 gm of oats flakes

![](/img/oatmilk2.jpg)

Preparation:
Place the oats in a glass bottle with the water
Soak overnight (in the fridge)
Blend and...pronto! (if you prefer a less creamy milk, you can strain it)
