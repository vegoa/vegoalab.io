---
layout: post
title:  "March 2016 meetup in Portugal"
date:   2016-03-30 14:52:41 +0100
categories: meetups
author: "Geraldine Starke"
---

From the 19th of February to the 15 of March 2016, we had the first Vegoa meetup here in Algarve. It was the opportunity to meet, discuss and work on the project.

![](/img/beach.jpg)

We also planned that after this meetups we would be able to define the Vegoa Manifest, set up the website, officially create the cooperative and scout for land as well as start getting new members involved to take it to the next level.

It was an amazing and exhausting month meeting wonderful and passionate people. We had great times going for hikes, scouting land, having meetings on the beach, enjoying cooking together and having discussions in front of the fireplace. Of course it was not just about having fun, we also made great progress together, debated about lots of topics, shared experiences, drafted the Manifest, prepared the website’s sitemap and came to conclusions about what Vegoa stands for.

![](/img/circle.jpg)

By having everyone’s input, it really helped gathering experience and information, understanding individual priorities and taking into account the main concerns as well as finding solutions to be able to welcome everyone. Thanks to all that, we were able to define precisely how Vegoa is going to work and handle every single aspect of it.

![](/img/monchique.jpg)

After this first meetup, we successfully managed to write down the official Manifest, launch the website and started to fill it with all information about Vegoa that we prepared during the meetup. We also found several land opportunities, like the Vegan Hills for which we are now actively searching families to join. We found a professional accountant and are now in the process of creating the main cooperative. Some members already moved permanently to Portugal and are continuously working to make the Vegoa project a reality.

![](/img/resto.jpg)

What started as a project is now becoming a movement. The interest and motivation are growing from day to day.

We will now organise vegan saturdays, every weekend, to show interested people the land Vegan Hills and meet us around a nice picnic.

More info about this events on the facebook page.
