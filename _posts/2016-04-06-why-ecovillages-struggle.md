---
layout: post
title:  "Why ecovillages struggle?"
date:   2016-04-06 22:15:02 +0100
categories: vegoa
author: "Tiago Pita"
---

I get asked frequently what will people live off when living in Vegoa; I want to answer that question by explaining why Vegoa is different from any other eco-community project.

Vegoa was deeply thought to be a minimal system that gives people the maximum freedom so they can focus on their passions and activism; in most ecovillages there is a centralised decision making process that approves or disapproves initiatives, at Vegoa there is no central committee or long voting system by people that don’t understand what you are proposing.

It’s easy to think that we need a central voting system, a leader or an elected representative because is easy to understand, but that never worked before so why should we create a new community with the same old controlling systems?

When studying other ecovillages and communities we noticed a pattern of disappointment with how priorities are decided and how people feel they have to trade their freedom for living in community, specially when communities get bigger.

None of us wanted to trade freedom for anything else, the same way we fight for animal freedom we fight for human freedom.

So how do we give freedom to everyone and still know what our priorities are?

Well we just let people decide in what they want to work, we do that by joining working groups that we call Organisations, Vegoans join the groups they feel they have knowledge to help with and have passion for. For example, I joined the “Vegoa Communications” because I have professional experience with communication and like to do it, I was not forced and can stop at any time. I can start my own organisations as well, I plan to work in a building organisation to help everyone building their houses and share the techniques with the world.

Another question we get regularly is: What about if someone does nothing?

When I help people building they can pay me with Vegoa coin “Vegetas” that they receive every month even if they don’t work. And now you are thinking that it makes even less sense because everyone receives free money.

When we give everyone a basic income we are guaranteeing people have more financial freedom to risk in new initiatives without prioritising money, they can sell their products and services cheaper to the community because the community gives them a basic income.

It’s easy to think that people will not work if they get free money, but is not that the beginning of a society where money is not needed? Why do we tend to think about the worst of humans instead of the best?

If we see Vegetas as a voting tool where people give their vegetas to the Organisations that have the best initiatives in the community so they improve their services or products then you can understand that Vegoa is more democratic and fair than a commune.

Organisations at Vegoa are not for profit, so people will not be working to make money, they will be working because they love what they do or they want to help the community, Vegoans can then reward those people by giving them their vegetas.

Vegoans can as well trade with the outside world with Euros and save enough to travel or other services that they may need from the outside world.

At Vegan Hills, our first village, we will have several organisations offering services to the outside world; for example we will have a Health Retreat with several activities where vegoans can work if they need euros, I plan as well to help people outside the community building sustainable housing.

We want to be 100% self-sustainable and want to produce everything within Vegoa but we are not naive thinking it will be easy, we know it will take a long time; we want to start by being energy and food self-sustainable. Using Vegetas is easy for us to know what products are produced 100% in the village because we will only be able to sell those products in Vegetas or otherwise someone will be losing euros.

The reason it sounds complicated is because we tend to compare it with our existing system and my explanation does the same, for that reason I want to explain again in a very simple sentence:

Vegoans have the social and financially freedom to start any initiatives with other Vegoans as long as it respects the Vegoa Manifest.

Ecovillages struggle because they don’t have freedom, they need to decide everything in group and the majority forces people into decisions that are not theirs, this results in members leaving or being constantly blocked.
Vegoa will grow to be a big community once we start with our initial project “Vegan Hills”. You can join us now to be part of this amazing project.
