---
layout: post
title:  "Make your own toothpaste!"
date:   2016-04-06 18:52:34 +0100
categories: degrowth
author: "Simona Vinati"
---

No toxic ingredients for you and for the environment! It is very easy to prepare and you can experiment with variations...Children will have fun as well preparing it :)

Usually I prepare a small glass jar of it, and it lasts for a couple of months.

The recipe is Inspired by an amazing project in Greece: Free&Real (which stands for Freedom of Resources for Everyone Everywhere & Respect, Equality, Awareness and Learning, Creating a School of sustainability and self-sufficiency...we are on the same track, arent we? ;) website: telaithrion.freeandreal.org )

![](/img/toothpaste.png)

NATURAL TOOTHPASTE

* 3 tablespoons white clay powder OR 2 tablespoons white clay powder and 1 tablespoon baking soda

* 1 tablespoon aloe vera natural gel OR melted coconut oil OR simply water

* a few drops of essential oil of choice (mint, sage, lemon, rosemary…..) AND/OR a tea spoon of dried and powdered herbs (sage, mint, rosmery…)

* recycled jar or toothpaste dispenser

mix dry and wet ingredients in order to create a paste of the consistency of toothpaste, add a few drops of essential oils of your choice (I like mint and sage).

Mix well and store in the recycled jar & enjoy!
