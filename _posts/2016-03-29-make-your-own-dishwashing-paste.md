---
layout: post
title:  "Make your own dishwashing paste!"
date:   2016-03-29 09:11:16 +0100
categories: degrowth
author: "Simona Vinati"
---

Following the motto of one of the inspiring projects I came across (actually in my hometown in Italy) “The only sustainable growth is Degrowth”.

I am starting a series of blog posts about natural sustainable daily care: recipes to make natural products (vegan of course) and recipes to  “recycle” ingredients in the kitchen.

Why? Low impact for the planet, easy ingredients, and packaging reduction!

Have fun :)

![](/img/dishwasher.jpg)

NATURAL DISHWASHING PASTE

* 4 whole lemons, pitted

* 100 gm fine sea salt

* 50 gm baking soda

* 200 ml white distilled vinegar

* 100 ml water

(if you really really need it to foam: add 50 gm of finely grated "Marseille" soap at the end of the procedure)

Cut the lemons (including the zest) very fine. Add salt and baking soda, and blend until smooth.

In a medium pot bring water and white distilled vinegar to boil. Add the mixture and boil gently for about 15 minutes, stirring.

Let it cool down and then store in a recycled glass jar.

Use by pouring the “cream” directly on the sponge OR a recycled dispenser.

It works well also to clean the oven and greasy surfaces.

![](/img/dishwasher2.jpg)
