---
layout: post
title:  "Introducing the Vegan Hills"
date:   2016-04-07 21:12:37 +0100
categories: vegan-hills
author: "Geraldine Starke"
---

Unlimited space, green valleys, flower beds, beautiful hills, little walking paths, flat hill tops with amazing views on preserved nature, that’s how I would describe the Vegan Hills in a few words.

<iframe width="560" height="315" src="https://www.youtube.com/embed/n15NKJQgmHU" frameborder="0" allowfullscreen></iframe>

We saw multiple lands but this one really seems ideal to start the first Vegoa project. Because of it’s huge size, 100 hectares, we will be able to realize multiple projects on it. We plan to farm using permaculture techniques, build our eco-houses using various sustainable methods, create a health center and a camping space to receive guests, have an animal sanctuary and many more exciting projects.

We aim to become self-sustainable, and want to achieve food and energy sustainability on this first land. It will take time before we are totally sustainable but we will really work towards this goal. We will experiment multiple techniques to farm, build and produce energy and we will organize events where we hope to have experienced people joining and sharing their knowledge with us.

![](/img/pano.jpg)

Another interesting aspect of this land is that it is located in the Algarve, a very touristic area, attracting loads of people every year. The Vegan Hills are situated very close to the west coast, in fact only 8 km by bike. We have several eco-tourism projects like a health center including juicing, healthy foods, yoga and so on, as well as a camping space to welcome tents, caravans and maybe in a near future build some tipis or other original accommodations.

The great climate here will allow us to grow a lot of fresh and colorful foods, like delicious avocados, bananas, figs, peaches, prunes, oranges, lemons, khakis and even have a tropical greenhouse for tropical deliciousness. We also plan to grow grains like quinoa, wheat or rice as well as hemp which we can use for clothing for example.

We are also really excited to start building our own houses, experience different techniques and make them in our image. That’s what’s amazing in the Vegoa Project, we are all able to let our creativity run wild and protect our individuality and in at the same time build something great with other people.

A lot of project ideas can blossom here and we would love to have new members joining and see them realizing their dreams.

Individual freedom is one of the most important aspect of Vegoa, and none of the above mentioned project ideas will be forced on anyone. We created the concept of organizations to allow people that are interested in a project to gather together in working groups and make it happen. We believe that freedom will give the opportunity to people to thrive, realize their projects and give their best and that can only be healthy for the community.

It will be the beginning of a new adventure and we are really looking forward to it. You can find more details on the Vegan Hills here.

We are now organising Vegan Saturday’s every weekend to show the land to interested people.

We are really excited to meet the new families that want to join us in this project.

![](/img/tree.jpg)
