---
layout: post
title:  "The beginning of a Vegan Community"
date:   2016-03-29 23:03:49 +0100
author: "Kati Anton"
category: news
---

Vegoa and how an alternative living style can become reality in the south of Portugal.

The adventure started about four months ago with a Facebook post from Tiago inviting other vegan entrepreneurs to create a vegan community somewhere in Europe.

Soon we narrowed it down to the south of Portugal for the beauty of nature, the weather and the relatively lax laws on immigration. The response and the speed in which this group was growing was overwhelming and soon we hit 1000 members. Around this time the first pioneers started moving to Portugal and the first two weeks of March 2016 twenty people met in Aljezur and started discussing the values of Vegoa, the possibilities to become self-sustainable and the ways the community should be organized.

Already before the meeting started it was clear to all active members that Vegoa should be more than a place where vegans around the world could be living and working together in a cruelty-free and sustainable way. It should also be a place where people's creativity can thrive, where everyone's dream projects could be realized with the support of the community and where free thinking is strongly encouraged. Thus, the idea of a basic income paid with Vegoas own coin that is used for trading within the community came up.

![](/img/night.jpg)

With this in mind the initial group set up the manifest of Vegoa with the basic principles that govern the future community, as well as a set of Q&A, which by no means is exhaustive and inviting to ask many other questions that we might or might not yet have an answer to. Parallel to creating the website the group has been looking for land that can be used not just for housing and farming but also to create jobs and a headquarter where the community’s heart will be.

The next steps will be organizing fundraising campaigns in order to buy enough land that will allow new members to come and settle in Vegoa. With the help of people that share the identified principles and values Vegoa will transcend from an idea into a place. Many skills are needed to help with finances, farming, building and communication, so Vegoa allows to share ideas and ask questions in order to make Vegoa come true.

Your Vegoa
