![Picture](/img/mountains-top.jpg)

* * *

*   **Why?** Vegoa wants to lead by example promoting Veganism and Sustainability 
*   **How?** Vegoans will live in cohousing projects, farm community gardens, create Vegan associations, promote vegan events and local activities
*   **What?** Vegoa is a vegan transition movement, vegan community and platform for vegan initiatives.
*   **Where?** South of Portugal
*   **Social Organisation** - Shared Resources (Commons), Transparency, Decentralisation, Equality
*   **Values** - Veganism, Sustainability, Freedom

* * *

## FAQ

*   **How do I join?**
    *   You may want to visit Vegoa before you decide to join. Once you decide to join you will be asked for a one time donation of 500€ that will help Vegoa welcome you and continue promoting its mission.
*   **What will the 500€ be used for ?**
    *   Association expenses (statute cost, …)
    *   Association insurance (to cover any legal actions against outside world)
    *   Marketing to promote Vegoa to the outside world
    *   Create a better hosting structure for the newcomers (ie. HQ office, …)
*   **Is there any other fees?**
    *   No, there is no tax, fees or rents within the community. Of course, taxes owed to the Portuguese government need to be paid regularly.
*   **Will I receive a house?**
    *   There will be different cohousing projects that you can join; existing ones or projects needing your financial, physical and intellectual help. Once the project has taken off you can rent accommodation at Vegoa or share a house with members of the cohousing project while building it.
*   **How do I sustain myself and family when in Vegoa?**
    *   We believe financial security is a human right, for that reason we actively support collective entrepreneurship and offer an unconditional basic income to every Vegoan.
*   **What if the project fails?**
    *   Financial sustainability and social organisation are very important for Vegoa resulting in a safe community environment. But in case unforeseen situation forces the project to finish your house, the plot of land for your house and your belongings are owned by you under the Portuguese law.
*   **What happens to the land? Will it be split? Will the plot where my house is on top belong to me?**
    *   Yes, the plot (house space) belongs to you.
*   **How fast can I move? **
    *   You can move as fast as you want, but please contact us before you do. This will allow us to help you feel welcome and get settled the best way possible.
*   **Are kids welcome?**
    *   Kids are very welcome and an important part of Vegoa future.
*   **Can I bring my paw friends?**
    *   Of course :)
*   **Can someone be expelled?**
    *   Vegoans can only be expelled from the community if they do not respect the Vegoa manifest and even then only with the approval of 90% of Vegoans. This does not mean that they lose their property. 
*   **Why should I join?**
    *   Everyone can join for different reasons, Vegoa is a community and platform for every vegan to thrive. 
*   **What commitment is expected from me in the community? (any minimal hours to commit?)**
    *   There is no minimal hours or expected commitment. You will be required, however, to accept the manifest. 
    *   Every member can choose whether or not to join any organisation and work on it. Every member can also create their own organisation.
    *   Every member is free to volunteer or help the community with its organisation.
    *   Individual creativity is being encouraged by allowing each Vegoan to live with a basic income without imposing any unnecessary rules.
