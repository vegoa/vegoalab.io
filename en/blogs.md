---
layout: page
title: Blogs
permalink: /en/blogs/
---

* * *

We are really proud to publish the Vegoans blogs in Vegoa. This wonderful project is now moving forward really fast and becoming a reality. Through this blog, Vegoans will share their exciting experience and let you become a part of it already.

* * *

![Picture](/img/lunch.jpg)

### Blog Posts

{% for post in site.posts %}
  {% if post.category != "news" %}
  <li>
    <a href="{{ post.url }}">{{ post.title }}</a>
  </li>
  {% endif %}
{% endfor %}
