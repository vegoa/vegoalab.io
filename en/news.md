---
layout: page
title: News
permalink: /en/news/
---

{% for post in site.posts %}
  {% if post.category == "news" %}
  <li>
    <a href="{{ post.url }}">{{ post.title }}</a>
  </li>
  {% endif %}
{% endfor %}
