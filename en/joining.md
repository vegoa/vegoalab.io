---
layout: page
title: How can I join Vegoa?
permalink: /en/joining/
---

* * *

Vegoa is a transition movement and community, we have several parallel projects that are *villages* or other co-housing options. These projects are all in the same area of Vila do Bispo, Portugal, and only about 30 minutes from each other.

Soon we will be listing those initiatives and you can find more details about them. For now, we are just initiating them and did not acquire a property yet. We need your help to make it possible.

* * *

## Joining process

You may want to visit Vegoa before you decide to join, to do so please [contact us](/en/joining/contact/).

To be able to join Vegoa you have to accept its [manifest](/en/manifest/). Once you decided to join you will be asked for a one time donation of 500€ (per person) that will help Vegoa welcome you and continue promoting its mission.

These 500€ will be used for:

* Association expenses (statute cost, …)
* Association insurance (to cover any legal actions against outside world)
* Marketing to promote Vegoa to the outside world
* Create a better hosting structure for the newcomers (ie. HQ office, …)
* Wage for the professional accountant

* * *

## Here are the current co-housing projects

**[Vegan Hills](/en/joining/vegan-hills/)** <span style="line-height: 1.6;">- Plot of 100 hectares that can accomodate farming, living, a sanctuary and a retreat with space for guests. Very fertile land, existing trees, water and 15 min from the sea. Requires initial financial contribution.</span>

**50 room/families co-housing** - Very small rent in big community building with 2 big industrials kitchens and possibility to build a resident kitchen per floor, big rooms with family suites, possibility to use temporary until joining a farming project or another cooperative.

**Vegan Beach** - Valley property that finishes almost at the beach where we plan to create several retreats, permaculture school and beach bar. 

This area of Portugal has several farming properties available for us to buy. We want to create the platform to welcome many vegans.

Please hold on until we share more information, we are getting many requests and need more Vegoans to help us handling all the newcomers.
