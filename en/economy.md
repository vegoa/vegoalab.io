---
layout: page
title: Economy
permalink: /en/economy/
---


* * *

Vegoa aims to have a self-sustainable economy that prioritises equality between all Vegoans. We achieve it by introducing a basic income and our own currency that incentivises to buy and sell inside the community. Simultaneously when buying a product or service Vegoans are voting in community initiatives. This way we create a vegan sustainable economy where we can guarantee that all products are created by the community and the values it shares.

* * *

## Basic Income

1.  Vegetas are co-created by all Vegoans.
2.  Every Vegoan receives a monthly basic income in the Vegetas currency.
3.  Every year the basic income grows 10% to guarantee equality (or ~0.73% monthly).
4.  The basic income results in an organic voting system where vegetas are naturally distributed to the organisations whose work is the most required/appreciated.

## Vegetas

1.  Vegetas can be used to buy products or services of a Vegoa organisation.
2.  Every Vegoan can receive more Vegetas participating in an Organisation that sells services/products in vegetas.
3.  Vegetas can also be used to create a new Organisation if needed.

## Trading

1.  Organisations can trade in Vegetas within the community and with euros/other currencies with the outside world.
2.  Organisations that trade products created outside Vegoa can charge euros for the product itself and Vegetas for the service of selling those products.
3.  Products made inside Vegoa should be traded using Vegetas.
4.  Services inside Vegoa are always priced in Vegetas for Vegoans but the organisations can offer their services in euros as well.

## FAQ

*   **Can I buy Euros with my Vegetas?**
    *   Vegetas can be traded with other currencies informally. No exchange system is available.
*   **Can non-Vegoans have/buy Vegetas?**
    *   No, Vegetas are only co-created from vegoans and belong to vegoans.
*   **Do kids get a basic income?**
    *   Yes, kids get a basic income so we don't create an age inequality.
*   **Does everyone get the same basic income?**
    *   Every Vegoan get the same basic income amount.
*   **Are all products inside Vegoa sold in Vegetas?**
    *   It is up to the Organisations to decide if it is financially sustainable to buy in euros and sell in Vegetas but all services and products created inside Vegoa should be sold in Vegetas.
*   **How do you define if a product is created in Vegoa or not?**
    *   We do not need to define it because there are several benefits for organisations to sell their products in vegetas. 
*   **If I have my own business does it have to become part of Vegoa?**
    *   No, Vegoa is just a recommendation. Your own business will stay yours.
*   **Why will the basic income get a yearly 10% growth?**
    *   Thanks to the 10% growth every year, the relative amount every member owns will be balanced.
    *   The 10% growth is based on the average lifespan of human-beings.
    *   As an example nowadays we use taxes to redistribute money from the lucky people to the less lucky ones that frequently find solutions to avoid them.
*   **My organisation receives euros and vegetas, can I receive both as an income?**
    *   As long as your organisation receives euros, as a member, you can suggest and decide all together a certain amount of your income in euros and another amount in vegetas. This makes sense as long as you need euros to get something from outside that we don’t have yet in the community.
*   **Why should I use vegetas if I have the choice to use any other currencies?**
    *   By using vegetas you will contribute to a vegan economy and therefore support vegan people and their values. It’s like a better version of the vegan stamp that ensure end-to-end cruelty-free exchanges.
    *   Vegetas is a decentralized digital currency (crypto-currency) not issued or controlled by any governments or financial world.
    *   Therefore no VAT or any taxes are applied to the transactions or incomes payment between members/organisations.
*   **How can I use my Vegetas?**
    *   Various softwares, such as web and mobile apps will be available to exchange vegetas between members.
*   **Does the Vegoa economy naturally tend to a sustainable ecosystem and influence its members and organisations to be sustainable? Is the Vegoa economy consistent with a sustainable lifestyle?**
    *   Nowadays, in our society, countries unilaterally or with consensus decide how they will use their global budget with taxes/private money creation and mainly  investing in non-sustainable energies following their own interests.
    *   In Vegoa, there is no such things as a global budget, every Vegoan gets their basic income and decide at their own scale which investment or not they want to follow. Therefore each Vegoan will naturally prefer to pay less and get more that will end up with sustainable decisions.
