---
layout: page
title: Organisations
permalink: /en/organisations/
---

* * *

A Vegoa Organisation is a working group of Vegoans that get together to offer a service, a product or a solution to a problem or need. They are a vital part of Vegoa decentralised structure.

* * *

## 4 types of Organisations

1.  **Open Organisations** - Every Vegoan can join an Open Organisation at any time, these Organisations manage common resources or common services like social commons.
2.  **Co-housing** -  Created to manage housing and habitational projects, can handle every aspect of communal living. Some co-housing organisations may have limits based on the availability of houses. Please be aware that you have to either join or create your own cohousing organisation in order to have a land where you can live.
3.  **Services and Products Organisations** - Similar to regular cooperatives, these Organisations can be initiated by a group of Vegoans to offer a service or product inside or outside the Vegoa community. Members of these organisations collectively decide when to invite new members to join. Vegoa promotes entrepreneurship to guaranty self sustainability of the Vegoa Project.
4.  **Land Management Association** - These organisations manage land plots and everyone that uses the land decide how to self-manage it.

## Basics of an Organisation

1.  Vegoa Organisations are not for profit but guarantee wages to members with the wage values being decided by everyone in that Organisation collectively; 
2.  Members of an Organisation collectively decide what management methodology to use: e.g. holacracy, democratic decision making or even regular hierarchy between others forms of organisation.
3.  Organisations are 100% transparent and information about it can be requested by any Vegoan at the end of each quarter. Vegoa incentivises Organisations to share all possible data online.
4.  Knowledge is shared for free between organisations and Vegoans inside the Vegoa Community; organisations are not forced to spend resources required for making the knowledge available, but are encouraged to do so.

## FAQ

*   **How do organisations decide how much money to pay their members?**
    *   Every member can suggest how much other members should earn, collectively they decide how much each other can get and when.
*   **As a Services and Products Organisation, can I buy a land ?**
    *   No, land is managed by a Land association of people that use the land.
*   **Does an Organisation need to be registered legally in Portugal or other country?**
    *   No, Vegoa organisations are just informal group organisations. However, when trading with the outside world and/or when the Organisation is of big size, registering a legal entity in Portugal (such as an association) should be considered.
*   **How are the organisations managed?**
    *   Their members decide, they can use any management technique at their own choice (this include money management as well). 
    *   Holacracy is the recommended form of organisation, with this methodology all the members have an equal say.
*   **Is there a retirement scheme available?**
    *   It is up to each Organisation to create one. Vegoa community should develop a Social commons system where children or the elderly are taken care of by everyone. Every Vegoan including the elderly will continue to receive the basic income that is co-created by everyone to offer basic rights to every Vegoan.
*   **I have an organisation and sell a product that needs outside raw materials and fee paid in euros, how can I be able to sell my product in vegetas?**
    *   If your product’s fee and raw materials are entirely bought in euros therefore it is easy to believe that you have no other choice than selling your product entirely in euros.
    *   However as the community grows and more organisations offering products/services are created, you can tend to convert your product price entirely in vegetas.
    *   To encourage it, organisations can expose their expenses regularly to the community so other members/organisations know what is necessary to produce for the community to become self-sufficient.
