---
layout: page
title: Vegan Hills
permalink: /en/joining/vegan-hills/
---

![Picture](/img/veganhills.jpg)

* * *

Beautiful enormous land with multiple hills and main valley connecting to smaller valleys where 30 to 40 houses/families can live and be self sustainable with multiple farming projects a health and educational retreat and a space for events. With natural houses and natural landscape this location should be an example to other Vegoa Villages.

* * *

<iframe width="560" height="315" src="https://www.youtube.com/embed/7KYsubmayO8" frameborder="0" allowfullscreen></iframe>

## Land characteristics

*   **Land size**: 100 hectares
*   **Capacity**: 30-40 houses
*   **Price**: 425,000€
*   **Location**: West Algarve
*   **Nearest village**:  1.6km
*   **Nearest city**: Lagos - 40 minutes by car
*   **Nearest beach:** 8km by bike

## Land Vision

We imagine a land full of food, flowers and trees, a place of small and infinite walkable ways and natural buildings. As the first Vegoa project we choose to make self-sustainability our priority and want to produce more than 100% of what we use. We want it to be a place that serves an example of a cruelty free world where people help each other and all the animals. A place that can receive people for events and health holidays.

## Reasons we choose this land

There are so many reasons that is hard to choose the first one to list; it's beautiful with valleys and plan hill tops and perfect conditions to farm and store water. Few minutes by bicycle to the beautiful and unspoiled west coast of Algarve. 1 million square Meters of paradise.

## Self-sustainability

The theme of Vegoa first land project is self-sustainability. After analysing other Eco-villages we learned that none reaches food and other resources independence. We think we can do it with this proposed plot. We plan to build natural houses using local and widely available materials and prioritise food productions trying to produce more than the residents can consume looking at other needs like clothing and linen production using plant fibres.

## Organisations and income creation

Independency does not come only from food production but from our financial freedom, we should require:

*   Health Retreat
*   Cantine
*   Animal Sanctuary
*   Natural pool/lake
*   Camping space
*   Food co-op
*   Several farming organisations
*   Events

## Vegan Hills manifest

1. As Members of vegan hills are also members of Vegoa they respect the Vegoa manifest.
2. The use of the land of vegan hills has to be sustainable and ecofriendly.
3. The resources on the land (cork, fruits, etc.) that already existed in the moment of acquisition of the land can be sold unless 90 % of residents of vegan hills oppose the sale. The profit of the sale (price minus the labor of extracting the resource) goes to Vegan Hills.
4. A group of people that wants to invest as a family and share the costs between them that consists of more than two adults should consider splitting in more than one family.
5. The materials for building the houses should be derived from the land itself, sustainable or recycled materials as possible practicable.
6. Vegan hills strives to be self-sufficient.
7. If a member of vegan hills wants to leave the project the initial investment of this person for buying the land will be refunded by the new person that takes over the place of the leaving member.
8. If the leaving member has built a house on the land it can be sold to the successor. However, the residents of vegan hills have the right to put a cap on the price of the house.
9. In case of death the house of the deceased can go to the inherent who has to accept the vegan hills manifest in order to be able to live in vegan hills or it can be sold according to clauses 5 and 6.
10. Decisions of a resident of vegan hills concerning the use of land can only be blocked by a majority of 90% of the residents.
11. The manifest of vegan hills can only be changed by a majority of 90 % of the residents of vegan hills.
12. Every Vegoan is free to decide about their way of living without consulting other members and can only be blocked by 90 % of Vegoans.
13. Each new resident needs to go through a trial time of 6 months onsite, after which they automatically become a resident unless a third of the residents disapprove in an official vote with the new member joining.

## FAQ

*   **How can I join?**
    *   To join the land you have to first join Vegoa. Please refer to the [joining page](/en/joining).
    *   Then please fill the [this form](http://goo.gl/forms/ufr0wB0U9n) or [contact us](/en/joining/contact) to organize a visit.
*   **Why only 30 to 40 families/houses?**
    *   Land is a limited resource and to achieve self-sustainability we need to limit the number of houses we can build on the land.
*   **How much will my house cost?**
    *   Because we want to use local materials, we want to build houses on a very low budget of a couple of thousand euro you may decide to spend more based on the materials you choose
*   **How much do I have to pay for the land? When? How?**
    *   You have to pay 10,000€ .
    *   The money will be paid into a registered association. The payment will be arranged directly to a bank account owned by the association.
    *   We already paid the deposit and part of the land. The payment is due once we have created the official association and bank account, and once we gathered the 30-40 families for the land. We hope to make it possible by September 2016.
*   **Can I visit the land before making any decision?**
    *   Of course, we would highly recommend everyone that is interested to first come meet us and visit the land.
*   **Is the payment 10k € payed per individual or family?**
    *   The payment required is per family/house, you can be single and own the house exactly as a family.
    *   You can decide to share the payment with a single person sharing the cost and house.
*   **What will I get for this money? What does this payment covers?**
    *   The payment covers the purchase of the land. 
    *   This payment does not include a house or anything else.
*   **Can I be expelled from this association?**
    *   Vegoa manifest only allows members to expel another member with 90% of the vote and only in case of a fundamental breach of the manifest. You can find more about it reading the [Vegoa manifest](/en/manifest/).
*   **Will I get my money back if I decide to leave?**
    *   If you decide to leave, you can get your investment back (10k€) and the cost of the materials of your house, if a new member joins and is interested by it.
*   **What happens if I have a very huge family, can I have a bigger house than other families?**
    *   We would not like to impose any restrictions but for sustainability reasons we would recommend building modest sized houses.
*   **How many houses can be legally built on the land?**
    *   There are building permissions for 2 buildings. We can apply for more building permissions but it will require at least a year.
    *   However we want to build houses "shelters" without infrastructure, which are in legal void in Portugal and do not require building permission if they are not connected to main services like public swage.
*   **Can people still join the land after the acquisition stage?**
    *   That’s a decision from everyone living in the land, if we all together decide to receive more people than we decide on how and when, but is common agreement that we don't. 
*   **Is my house mine under the Portuguese law?**
    *   Houses in this project are not considered “houses” under Portuguese law as they don't have a foundation. However, what you built on the land is yours under Vegoa manifest and as member of the association.
    *   (Other Vegoa projects will include regular housing under the Portuguese law)
*   **Is there a lease involved?**
    *   No

* * *

If you are interested in this land and would like to join, please fill out this form: 

[Google Form](http://goo.gl/forms/ufr0wB0U9n)

Or contact us if you need more information and want to arrange a visit:

[Contact US](/en/joining/contact)
