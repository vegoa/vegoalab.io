---
layout: page
title: Contact
permalink: /en/joining/contact/
---
* * *

Here are our contact details, feel free to reach us if you have any question.

* * *

**<big>Email:</big>** [geraldine@starke.fr](mailto:geraldine@starke.fr)

**<big>Facebook page:</big>** [facebook.com/vegoans](https://www.facebook.com/vegoans)

![Picture](/img/hiking.jpg)
