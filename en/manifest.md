---
layout: page
title: Manifest 1.1.0 - May 10th, 2016
permalink: /en/manifest/
---

* * *

Every member that wants to join the Vegoa community has to comply with the basic rules of the _Manifest_.

* * *

1.  All members joining _Vegoa_ are committed to a vegan lifestyle according to the [Vegan Society definition of veganism](https://www.vegansociety.com/go-vegan/definition-veganism)
2.  Each member of the _Vegoa_ community (hereinafter referred to as: _Vegoan_) gives their best effort to protect the environment and promote self-sustainability. 
3.  Each _Vegoan_ respects the rights and freedom of its fellow _Vegoans_.
4.  The rights and freedom of each _Vegoan_ inside the community shall only be limited to the extent that they interfere inappropriately with the rights and freedoms of other _Vegoans_. 
5.  There shall be no discrimination based on gender, age, religion, race, sexual orientation or disabilities between _Vegoans_.
6.  _Vegoans_ agree to respect criminal Portuguese law.
7.  A _Vegoan_ living in a private house built in a co-housing land cannot be expropriated and can trade their house.
8.  _Vegoans_ can only be excluded from the community when they fail to respect the _Manifest_ with a 90% vote.
9.  Every _Vegoan_ has the right to a _Basic Income_.
10.  The _Manifest_ can only be changed with a majority vote of 90% of _Vegoans_.
11.  Every organisation under Vegoa is free to decide their decision making process.
12.  Organisation’s manifests can not override this manifest or any of its clauses
13.  When joining or creating an organisation, its manifest as well as the Vegoa manifest have to be accepted and respected.

## FAQ

*   **How can I suggest an update to the Manifest?**
    *   You can improve the Manifest by suggesting a new version following the semantic MAJOR.MINOR.PATCH inspired by [Semantic Versioning](http://semver.org).
    *   However 90% of the present _Vegoa_ members have to approve any new changes to endorse a new version of the Manifest and release it.
    *   Given a version number MAJOR.MINOR.PATCH, increment the:
        *   MAJOR version when incompatible Manifest changes are made,
        *   MINOR version when rules in a backwards-compatible manner are added, and
        *   PATCH version when backwards-compatible consistency/typo fixes are done.
