---
layout: page
title: Contact
permalink: /fr/rejoindre/contact/
---

* * *

Voici nos coordonnées en détails, n'hésitez pas à nous contacter pour toute question.

* * *

**<big>Email :</big>**
[geraldine@starke.fr](mailto:geraldine@starke.fr)

**<big>Page facebook :</big>**
 [facebook.com/vegoans/](https://www.facebook.com/vegoans/)

**<big>Groupe facebook francophone :</big>**

[facebook.com/groups/leprojectvegoa/](https://www.facebook.com/groups/leprojectvegoa/)

![Picture](/img/hiking.jpg)
