---
layout: page
title: Vegan Hills
permalink: /fr/rejoindre/vegan-hills/
---

![Picture](/img/veganhills.jpg)

* * *

Magnifique et énorme terrain avec plusieurs collines et une vallée principale reliant plusieurs vallées plus petites, où 30 à 40 maisons/familles peuvent vivre et être autonome avec plusieurs projetcs de culture, une centre de soin et d'écucation et de l'espace pour des évènements. Avec des maisons naturelles et un paysage préservé, ce lieu servira d'example pour les autres villages Vegoa.

* * *

<iframe width="560" height="315" src="https://www.youtube.com/embed/7KYsubmayO8" frameborder="0" allowfullscreen></iframe>

## Characteristiques du terrain

*   **Taille du terrain** : 100 hectares
*   **Capacité** : 30-40 maisons
*   **Prix** : 400,000€
*   **Lieu** : Ouest Algarve
*   **Village le plus proche** :  1.6km
*   **Ville la plus proche** : Lagos - 40 minutes en voiture
*   **Plage la plus proche:** 8km en vélo

## Vision

Nous imaginons un terrain rempli de nourriture, de fleurs, d'arbres, une lieu avec de nombreux petits chemins infinis et des constructions naturelles. Comme premier project Vegoa nous décidons de faire de l'auto-suffisance notre priorité et voulons produire plus de 100% de ce que nous utilisons. Nous voulons faire de ce lieu un exemple d'un monde sans cruauté où les gens s'entraident ainsi que tous les animaux. Un lieu pouvant recevoir des gens pour des évènements et des vacances santé.

## Raisons de notre choix pour ce terrain

Il y a tellement de raisons qu'il est difficile d'en choisir un de la liste. Ce terrain est magnifique avec ses vallées et ses plateaux en hauts des collines et a des conditons parfaites pour cultiver et stocker de l'eau. Quelques minutes en vélo des belles plages préservées de la côte ouest d'Algarve. 1 million de mètres carrés de paradis.

## Auto-suffisance

Le thème du premier projet de terrain de Vegoa est l'auto-suffisance. Après avoir étudié d'autres éco-villages, nous avons appris qu'aucun d'eux n'atteint l'indépendance alimentaire et des ressources. Nous pensons pouvoir le faire avec ce terrain qui est proposé. Nous avons prévu de construire des maisons naturelles en utilisant des matériaux locaux et largement disponible et de prioriser la production alimentaire au delà des besoins des résidents afin de répondre à d'autres besoins tels que les vêtements et autres types de production utilisant les fibres des plantes.

## Permis de construire

Ce terrain possède 2 ruines avec permis de construire pour 2 bàtiments de 300 m2 chacun (600 m2 au total) qui peuvent être étendus à 1200 m2\. Ces bâtiments pourront être utilisés dans le futur pour des espaces communs.

## Organisations et création de revenus

L'indépendance ne vient pas uniquement de la production alimentaire mais aussi de notre liberté financière, nous devrons avoir :

*   Centre de santé
*   Cantine/restaurant
*   Sanctuaire animalier
*   Piscine/lac naturels
*   Espace de camping
*   Coopérative alimentaire
*   Plusieurs organisations de permaculture
*   Évènements

## FAQ

*   **Comment puis-je rejoindre ce projet de terrain ?**
    *   Pour joindre le projet Vegan Hills, vous devez d'abord devenir membre de Vegoa. Merci de vous rendre sur la page [rejoindre](/fr/rejoindre) pour cela.
    *   Par la suite il vous faut remplir [ce formulaire](http://goo.gl/forms/ufr0wB0U9n) ou nous [contacter](/fr/rejoindre/contact) pour organiser une visite.
*   **Pourquoi seulement 30 à 40 familles/maisons ?**
    *   La terre est une ressource limitée et pour atteindre l'autosuffisance nous devons limiter le nombre de maisons que nous pouvons construire sur le terrain.
*   **Combien va coûter ma maison ?**
    *   Parce que nous voulons utiliser des matériaux locaux, nous voulons construire des maisons à petits budgets de quelques milliers d'euros. Vous pouvez décider de dépenser plus en fonction des matériaux que vous souhaitez utiliser.
*   **Combien vais-je devoir payer pour ce terrain? Quand ? Comment ?**
    *   Vous devez payer de 10 000€ à 14 000€ en fonction du nombre de familles qui souhaitent rejoindre le village.
    *   L'argent sera versé à la coopérative officielle, de laquelle vous serez co-propriétaire à part égale avec les autres membres. La paiement sera versé directement au compte bancaire detenu par cette coopérative.
    *   Vous ne payerais jamais directement à un autre membre Vegoan et toutes les procédures seront gérées par un comptable professionnel.
    *   Le paiement est dû uniquement après la création officielle de la coopérative et de son compte bancaire, et lorsque nous aurons réunis les 30-40 familles pour le terrain. Nous espérons rendre cela possible dès juin 2016.
*   **Est-ce que je peux visiter le terrain avant de prendre une décision ?**
    *   Bien sûr, nous recommandons fortement à tout ceux intéressés de venir préalablement nous rendre visite et de venir voir le terrain.
*   **Est-ce que le paiement demandé de 10-14k€ est par personne ou par famille ?**
    *   Le paiement demandé est par famille/maison.
    *   Votre famille sera co-propriétaire du terrain avec les autres familles et vous serez propriétaire de votre maison à part égale entre tous les membres de votre famille.
*   **Que vais-je recevoir pour cet argent ? Que couvre ce paiement ?**
    *   Le paiement permet l'achat du terrain. Vous serez co-propriétaire de celui-ci avec les autres familles impliqués dans ce projet.
    *   Ce paiement n'inclut pas une maison ou quoi que ce soit d'autre.
*   **Est-ce que je peux être exclut de cette organisation ?**
    *   Le manifeste de Vegoa permet aux membres d'exclure un autre membre uniquement avec un vote à 90% et uniquement dans le cas d'un manquement fondamental au manifest. Vous trouverez plus d'information sur cela dans le [manifeste](/fr/manifeste).
*   **Est-ce que je récupère mon argent si je décide de partir ?**
    *   Si vous decidez de partir vous pouvez récuperer votre investissement (10k€) et le coût brut des matériaux de votre maison, si un nouveau membre rejoint le groupe et est intéressé par celle-ci.
*   **Que ce passe t-il si j'ai une très grande famille, puis-je avoir une plus grande maison que les autres familles ?**
    *   Nous ne souhaitons pas imposer de restrictions quelconques mais pour des raisons de viabilité nous recommandons de construire des maisons de taille modeste.
*   **Combien de maison peuvent-être légalement construitent sur le terrain ?**
    *   Ce terrain a des permis de construire pour 2 bâtiments. Nous pouvons faire la demande pour obtenir plus de permissions mais cela nécessite au minimum 1 an.
    *   Toutefois, nous souhaitons construire des maisons sans fondations qui sont dans un vide juridique au Portugal et ne nécessite pas de permis de construire.
*   **Est-ce que des personnes peuvent encore se joindre au terrain après son acquisition ?**
    *   C'est une décision que prendrons ensemble toutes les personnes vivant sur ce terrain, si nous décidons ensemble de recevoir plus de résidents alors nous devrons décider du quand et comment.
*   **Est-ce que j'ai la propriété de la parcelle sur laquelle se trouve ma maison ?**
    *   Vous êtes propriétaire de la coopérative qui détient l'ensemble du terrain et le manifeste vous donne des droits dessus.
*   **Ma maison m'appartient-elle selon la loi portugaise ?**
    *   Les maisons de ce projet ne sont pas considérées comme des "maisons" dans la loi portuguaise comme elles n'ont pas de fondations.Toutefois, ce que vous construisez sur le terrain vous appartient selon le manifeste de Vegoa et en tant que partenaire de la coopérative.
    *   (D'autres projets Vegoa inclurons des logements standards selon la loi portugaise)
*   **Est-ce que ce projet implique un crédit ?**
    *   Non
*   **Quel est le processus décisionnel qui a été choisi par l'organisation qui gère le terrain ?**
    *   La méthodologie décisionnel des organisations est décidée démocratiquement par ses membres en suivant [les principes des organisations Vegoa](/fr/organisations)

* * *

Si vous êtes intéressé par ce terrain et souhaitez vous joindre à ce projet, merci de bien vouloir remplir ce formulaire :

[Google Form](http://goo.gl/forms/ufr0wB0U9n)

Ou contactez nous si vous avez besoin de plus d'informations ou pour arranger une visite :

[Nous contactez](/fr/contact)
