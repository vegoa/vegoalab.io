---
layout: page
title: Manifeste 1.0.0
permalink: /fr/manifeste/
---


* * *

Chaque membre qui veut rejoindre Vegoa devra adhérer aux règles de base du manifeste.

* * *

1.  Tous les membres rejoignant Vegoa suivent un mode de vie vegan (sans cruauté envers les animaux ni exploitation de ceux-ci).
2.  Chaque membre de la communauté Vegoa (ci-après désigné comme Vegoan) se doit de protéger l’environnement et de favoriser l’autosuffisance.
3.  Chaque Vegoan respecte les droits et libertés des autres Vegoans.
4.  Les droits et la liberté de chaque Vegoan à l’intérieur de la communauté seront seulement limités dans la mesure où ils interfèrent avec les droits et la liberté d’un autre Vegoan.
5.  Il n’y aura aucune discrimination basée sur le sexe, l’âge, la religion, la race, l’orientation sexuelle ou les handicaps entre Vegoans.
6.  Les Vegoans acceptent de respecter la loi portugaise, en particulier les impôts et le droit pénal.
7.  Un Vegoan vivant dans une maison privée construite dans un terrain de co-habitation ne peut pas être exproprié et peut vendre sa maison.
8.  Un Vegoan peut seulement être exclu de la communauté s'il ne respecte pas le manifeste et avec un vote à 90%.
9.  Chaque Vegoan a droit à un revenu de base.
10.  Chaque nouveau membre doit passer une période d’essai de 6 mois, après quoi il devient automatiquement Vegoan sauf si un tiers des membres désapprouvent dans un vote officiel ce nouveau membre.
11.  Le manifeste ne peut être changé que par un vote majoritaire à 90%.

## FAQ

*   **Comment puis-je suggérer une mise à jour du Manifeste ?**
    *   Vous pouvez améliorer le Manifeste en suggérant une nouvelle version suivant la sémantique MAJEUR.MINEUR.CORRECTIF inspirée de la [Gestion Sémantique de Version](http://semver.org/lang/fr/).
    *   Toutefois 90% des membres actuels de Vegoa doivent approuver tout changement pour valider une nouvelle version du Manifeste et pour la publier.
    *   Pour changer le numéro de version MAJEUR.MINEUR.CORRECTIF, incrémentez :
        *   le numéro de la version MAJEUR lorsque des changements incompatibles avec la version précedente ont été fait,
        *   le numéro de la version MINEUR lorsque des changements compatibles avec la version précedente ont été fait, et
        *   le numéro de la version CORRECTIF lorsque des corrections de typo et de cohérence, compatible avec la version précedente, sont effectuées.
