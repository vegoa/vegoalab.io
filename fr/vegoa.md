---
layout: page
title: Le Projet Vegoa
permalink: /fr/
---


![Picture](/img/mountains-top.jpg)

* * *

*   **Pourquoi ? **Vegoa veut promouvoir par l'exemple un mode de vie Vegan et durable
*   **Comment ?** Les Vegoans ont pour projets des communautés de logements, des jardins d'exploitation agricole, la création d'une coopérative vegan et favoriseront les évenements vegan ainsi que les activités locales
*   **Quoi ?**  Vegoa est un mouvement vegan de transition, une plate-forme de communauté vegan pour des initiatives vegan
*   **Où?** Au Sud du Portugal
*   **Organisation sociale ?** - Ressources partagées, Terrains communs, transparence, décentralisation, égalité
*   **Valeurs** -  Véganisme, Durabilité, Liberté

* * *

## FAQ

*   **Comment rejoindre ?**
    *   Vous pouvez visiter Vegoa avant de vous décider à nous rejoindre. Une fois votre choix fait, il vous sera demandé de faire un don unique de 500 € pour devenir membre, et qui permettra à Vegoa de vous acceuillir et de continuer à promouvoir sa mission.
*   **À quoi serviront les 500 € ?**
    *   Dépenses de l'association (coût de création des statuts, …)
    *   Assurance de l'association (afin de couvrir des éventuelles actions légales avec le monde extérieur)
    *   Marketing pour promouvoir Vegoa au reste du monde
    *   Créer une meilleure structure d'acceuil pour les nouveaux arrivants (ex: bureau du siège social, …)
*   **Y a t'il d'autres frais ?**
    *   Non, il n'y a pas de taxes, frais ou loyers au sein de la communauté. Évidemment les taxes dûes au gouvernement portugais devront être payées régulièrement.
*   **Vais-je recevoir une maison ?**
    *   Il y aura différent projets de co-habitations que vous pourrez rejoindre. Certains déjà existants et d'autres qui nécessiteront votre investissement financier, physique et intellectuel. Une fois que le projet à démarré vous pourrez louer des hébergements à Vegoa ou partager une maison avec des membres du projet de co-habitation pendant que vous le construisiez.
*   **Comment puis-je subvenir à mes besoins et ceux de ma famille en étant à Vegoa ?**
    *   Nous pensons que la sécurité financière est un droit humain, c'est pourquoi nous soutenons activement l'entrepreunariat collectif et offrons un revenu de base inconditionnel à tous les Vegoans.
*   **Que se passe t-il si le projet échoue ?**
    *   La viabilité financière et l'organisation sociale sont très important pour Vegoa et résulte en un environnement communautaire sécurisant. Mais dans le cas où des situations imprévues nous forcent à arrêter, le terrain de votre maison et vos affaires personnelles vous appartiennent selon la loi portuguaise.
*   **Que ce passera t-il avec le terrain ? Sera t-il divisé ?**
    *   Oui le terrain sur lequel se trouve votre maison vous appartient.
*   **Quand puis-je emménager ? **
    *   Vous pouvez emménager dès que vous le souhaitez, mais s'il vous plaît contactez nous avant. Ceci nous permettra de vous acceuillir au mieux.
*   **Les enfants sont ils les bienvenus ?**
    *   Les enfants sont plus que bienvenus et représente une partie importante du future de Vegoa.
*   **Puis-je emmener mon ami à pattes ?**
    *   Bien sûr :)
*   **Une personne peut-elle être exclue ?**
    *   Un Vegoan peut uniquement être exclu de la communauté s'il ne respecte pas le manifeste et même dans ce cas seulement avec l'accord à 90% des autres Vegoans. Cela ne veut pas dire qu'il perd sa propriété.
*   **Pourquoi devrais-je rejoindre Vegoa ?**
    *   Chacun peut rejoindre Vegoa pour différentes raisons. Vegoa est une communauté et une plateforme où tout vegan peut prospérer.
*   **Quel implication est attendue de moi dans la communauté ? (heures minimales d'implication ?)**
    *   Il n'y a pas d'heures minimum requis ou d'implication quelconque attendue. Il vous sera demandé, tout de même, d'accepter le manifeste.
    *   Chaque membre peut choisir de joindre ou non une organisation et de travailler dessus. Chaque membre peut également créer sa propre organisation.
    *   Chaque membre est libre de faire du volontariat ou d'aider la communauté et ses organisations.
    *   La créativité individuelle est encouragée en permettant à chaque Vegoan de vivre avec un revenu de base sans imposer de règles non nécessaires.
