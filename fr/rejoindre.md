---
layout: page
title: Comment rejoindre Vegoa?
permalink: /fr/rejoindre/
---

* * *

Vegoa est un mouvement de transition communautaire, dans lequel nous avons plusieurs projets parallèles comme des "villages" ou d'autres sortes de projet de co-habitats. Ces projets sont localisés autour de Vila Da Bispo, Portugal, et dans un rayon de 30 minutes les uns des autres.

Rapidement nous allons lister ces initiatives et vous les détailler. Pour le moment, nous initions ces projets sans avoir encore fait l'aquisition de terrain. Nous avons besoin de votre aide pour rendre possible ces projets.

* * *

## Adhésion

Vous pouvez nous rendre visite avant de décider d'adhérer, pour cela merci de nous [contacter](/fr/rejoindre/contact).

Afin de pouvoir adhérer à Vegoa il faut accepter son [manifeste](/fr/manifeste). Une fois la décision prise de vouloir rejoindre Vegoa, une donation unique de 500€ (par personne) vous sera demandée et va permettre à Vegoa de pouvoir vous acceuillir et de continuer à promouvoir sa mission.

Ces 500€ seront utilisés pour:

*   Dépenses de la cooperative (coûts des statuts,...)
*   Assurance de la cooperative (pour couvrir toute action légale contre le monde extérieur)
*   Marketing pour promouvoir Vegoa au reste du monde
*   Créer une meilleure structure d'acceuil pour les nouveaux arrivants (ex: bureau du QG,...)
*   Salaire du comptable professionel

Une fois devenu un Vegoan, vous serez co-propriétaire à part égale de la coopérative avec les autres membres et recevrez un [revenu de base](/fr/economie) mensuellement.

Merci de noter que les 6 premiers mois après votre adhésion sont considerés comme une "période d'essai" et les membres de Vegoa peuvent s'opposer à votre adhésion. Si un tiers des membres s'y oppose, il vous sera demandé de quitter la communauté. Après cette période d'essai vous devenez officiellement un Vegoan. Toutefois, vous avez déjà droit à votre revenu de base pendant cette période.

## Voici les projets de co-habitats actuels :

**[Vegan Hills](/fr/rejoindre/vegan-hills)** - 100 hectares de terrain permettant la culture, l'habitation, un sanctuaire animalier et les projets d'éco-tourisme. Des terres très fertiles, arborées, avec de l'eau en abondance et à seulement 15 min de l'océan. Ce projet nécessite une contribution financière de départ.

**50 chambres/co-habitat familiale** - Location bon marché dans un grand immeuble communautaire avec 2 grandes cuisines professionnelles et la possibilité de construire une cuisine collective par étage, de grandes chambres/suites familiales. Option possible de logement temporaire, en attendant de rejoindre un projet de ferme communautaire ou tout autre coopérative.

**Plage Vegan** - Une propriété dans une vallée débouchant sur la plage où nous pouvons planifier de créer plusieurs possibilités d'éco-tourisme, une école de permaculture et un bar de plage.

Cette région du Portugal comporte plusieurs terres agricoles disponibles à l'achat. Nous voulons créer une plateforme pour accueillir un maximum de vegans.

Merci de patienter pour plus d'informations, nous avons beaucoup de requêtes et avons besoin de plus de Vegoans pour nous aider à prendre en charge les nouveaux arrivants.
