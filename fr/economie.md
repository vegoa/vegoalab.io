---
layout: page
title: Économie
permalink: /fr/economie/
---

* * *

Vegoa vise à avoir une économie autonome et durable qui priorise l'égalité entre tous les Vegoans. Ceci est réalisable en instaurant un revenu de base avec notre propre devise qui encourage à acheter et vendre à l'intérieur de la communauté. Ainsi, en achetant un produit ou un service, les Vegoans votent pour les initiatives de la communauté. De cette façon, nous créons une économie vegan viable où nous pouvons garantir que tous les produits sont créés par la communauté et selon ses valeurs.

* * *

## Le revenu de base

1.  Les Vegetas sont co-créés par tous les Vegoans
2.  Chaque Vegoan perçoit un revenu de base mensuel en devises Vegetas.
3.  Chaque année le revenu de base est augmenté de 10% pour garantir l'égalité (ou 0.73% mensuel).
4.  Le revenu de base peut s'assimiler à un système de vote où les Vegetas sont naturellement distribuées aux organisations où leur travail est le plus requit/apprécié.

## Vegetas

1.  Les vegetas peuvent être utilisés pour payer des produits ou services d'une organisation Vegoa
2.  Chaque Vegoan peut recevoir plus de vegetas en participant à une organisation qui vend des services /produits en vegetas.
3.  Les vegetas peuvent également être utilisés pour créer une nouvelle organisation si nécessaire.

## Échange

1.  Les organisations peuvent vendre en vegetas au sein de la communauté et en euros, ou autres devises, avec le monde extérieur.
2.  Les organisations qui vendent des produits créés à l’extérieur de Vegoa peuvent demander des euros pour le produit lui-même et des vegetas pour le service de vente de ces produits.
3.  Les produits crées au sein de Vegoa devraient être vendus en vegetas.
4.  Les services à l’intérieur de Vegoa sont toujours vendus en Vegetas pour les Vegoans mais ils peuvent aussi bien offrir leurs services en euros.

## FAQ

*   **Est ce que je peux acheter des Vegetas avec des Euros ?**
    *   Les Vegetas peuvent être officieusement échangé avec d'autres devises. Aucun système d'échange est cependant fournit.
*   **Est ce que les non-Vegans peuvent-il acheter/vendre des Vegetas ?**
    *   Non, les Vegetas sont uniquement co-crée par des vegoans et appartiennent aux vegoans.
*   **Est ce que les enfants reçoivent un revenu de base ?**
    *   Oui, les enfants reçoivent un revenu de base ainsi nous créeons pas d'inégalité d'ages.
*   **Est ce que tout le monde reçoit le même revenu de base ?**
    *   Tous les Vegoans reçoivent le même montant de revenu de base.
*   **Est ce que tous les produits au sein de Vegoa sont vendu en Vegetas ?**
    *   C'est aux Organisations de décider si il est financièrement possible d'acheter en Euros et vendre en Vegetas cependant tous les services et produits crées au sein de Vegoa doivent être vendu en Vegetas.
*   **Comment vous definissez si un produit est crée ou non au sein de Vegoa ?**
    *   Cela n'est pas nécessaire puisque ils existent une multitude d'avantaes pour les organisations de vendre leurs produits en Vegetas.
*   **Si je suis propriétaire d'une entreprise, est ce qu'il doit faire partie nécessairement de Vegoa ?**
    *   Non, Vegoa est juste une recommandation. Votre entreprise reste la votre.
*   **Pourquoi est ce que le revenu de base a une croissance de 10% par an ?**
    *   Grâce à la croissance des 10% par an, le montant relatif que chaque membre posséde sera réquilibré.
    *   La croissance des 10% se base sur l'espèrance de vie des êtres humains.
    *   Par exemple, de nos jours, nous utilisons massivement des systèmes de taxes comme moyen de redistribution de monnaies des plus chanceux aux moins chanceux qui fréquemment trouvent des solutions pour les éviter.
*   **Mon organisation reçoit des euros et vegetas, puis je recevoir les deux comme salaire ?**
    *   Aussi longtemps que votre organisation reçoit des euros, en tant que membre, vous pouvez suggèrer et décider tous ensemble un certain montant de votre salaire en euros et un autre montant en vegetas. Cela a du sens aussi longtemps que vous avez besoin d'euros pour obtenir quelque chose de l'exterieur que nous possédons pas encore au sein de la communité Vegoa.
*   **Pourquoi dois-je utiliser des vegetas si j'ai la possibilité d'utiliser toutes autres devises ?**
    *   En utilisant des Vegetas, vous allez contribuer à une économie entièrement Vegan et ainsi encourager les Vegans et leurs valeurs. C'est en quelque sorte une version améliorée du cachet Vegan qui guarantie les échanges Vegan de point en point.
    *   Le Vegeta est un système monnétaire décentralisé (crypto-monnaie) non distribué ou contrôlé par une instance gouvernementale ou le monde des finances.
    *   Ainsi pas de TVA ou autres taxes sont appliqués aux transactions ou paiements de revenus entre membres/organisations.
*   **Comment puis-je utiliser mes Vegetas ?**
    *   Plusieurs logiciels comme des applications web et mobile vont être mis à disposition pour échanger des Vegetas entre les membres/organisations.
*   **Est ce l'économie Vegoa tend naturellement à un écosystème durable et influence ses membres et organisations à faire de même ? Est ce que l'économie Vegoa est compatible avec un style de vie durable ?**
    *   De nos jours, dans notre société, les pays unilatéralement ou par censensus décident comment ils vont utiliser leur budget globalement à l'aide de taxes/création monétaire privée. Suivant leurs propres intêrets au détriment de leurs citoyens, ils investissents principalement dans des énergies non-durables.
    *   Dans Vegoa, il n'existe pas de budget globale, tous les Vegoans reçoivent leur revenu de base et décide à leur échelle quel investissement ou non ils souhaites suivre. Ainsi chaque Vegoan va naturellement préférer payer moins et obtenir plus, ce qui va conduire à des décisions durables.
