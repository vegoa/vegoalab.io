---
layout: page
title: Organisations
permalink: /fr/organisations/
---


* * *

Une organisation Vegoa est un groupe de travail regroupant des Vegoans qui souhaite offrir des services, un produit ou une solution à un problème ou besoin. Ils sont une partie vitale de la structure décentralisée de Vegoa.

* * *

## 4 types d'organisation

1.  **Organisations ouvertes** - Tout Vegoan peut rejoindre une organisation ouverte et à tout moment. Chaque organisation gérera les ressources et services partagés, comme par exemple les communs sociaux.
2.  **Co-habitat** - Ces organisations,créées pour gérer des projets de logement et d'habitation, peuvent traiter chaque aspect de la vie communale. Certaines organisations de co-habitat peuvent rencontrer des limites de disponibilité des maisons. Veuillez noter que vous devez joindre ou créer votre propre organisation de co-habitat afin d'aquérir l'endroit où vous souhaitez vivre.
3.  **Organisations de services et produits** - Semblables aux coopératives classiques, ces organisations peuvent être initiées par un groupe de Vegoans pour offrir un service ou un produit à l'intérieur ou en dehors de la communauté Vegoa. Les membres de ces organisations décident collectivement quand inviter de nouveaux membres à se joindre à eux. Vegoa favorise l'esprit d'entreprise pour garantir la durabilité du projet Vegoa.
4.  **Organisation pour la gestion de terrain** - Ces organisations gèrent des terrains, et chaque utilisateur d'un terrain décide de son auto-gestion.

## Les principes fondamentaux d'une organisation

1.  Les organisations de Vegoa ne sont pas à but lucratif mais garantissent à chaque membre un salaire décidé collectivement dans cette organisation.
2.  Les membres d'une organisation décident collectivement de la méthode de gestion qu'ils souhaitent employer: par exemple l'holacratie, le processus décisionnel démocratique ou même le système hiérarchique conventinelle.
3.  Les organisations sont à 100% transparentes et des informations les concernant peuvent être demandées par tout Vegoan chaque trimestre.Vegoa incite les organisations à partager toutes les données possibles en ligne.
4.  Les connaissances sont partagées gratuitement entre les organisations et les Vegoans à l'intérieur de la communauté Vegoa. Les organisations sont encouragées, sans obligation, à déployer les ressoures nécessaires pour rendre ces informations et connaissances disponibles.

## FAQ

*   **Comment est-ce que les organisations vont décider du montant d'argent qu'elles vont payer à leurs membres ?**
    *   Chaque membre peut suggérer combien les autres membres devraient gagner. Collectivement, les membres décident de combien ils vont gagner et quand.
*   **En tant qu'organisation de produits et services, est-ce que je peux acheter un terrain ?**
    *   Non, les terrains sont gérés par une association de gestion de terrains regroupant les personnes utilisant ceux-ci.
*   **Est-ce qu'une organisation nécessite d'être enregistrée légalement au Portugal ou dans un autre pays ?**
    *   Non, les organisations Vegoa sont uniquement des groupes de travail informels. Toutefois, lors d'échanges avec le monde extérieur et/où lorsqu'une organisation est de grande taille, s'enregistrer légalement au Portugal (en tant que coopérative par exemple) devrait être considéré.
*   **Comment les organisations sont-elles gérées ?**
    *   Les membres décident, ils peuvent utiliser toutes techniques de management selon leur choix (cela inclus également la gestion financière).
    *   L'holacracie est toutefois recommandée, avec cette méthodologie tous les membres ont leurs mots à dire.
*   **Y'a t'il un plan retraite ?**
    *   C'est à chaque organisation que revient la décision d'en créer un. La communauté de Vegoa devrait développer un système de communs sociaux où les enfants et les personnes agées sont pris en charge par tous. Chaque Vegoan, les personnes agées également, vont continuer à recevoir leur revenu de base qui à été co-crée par tous afin d'offrir des droits de base à tous les Vegoans.
*   **J'ai une organisation et je vend un produit qui nécessite des matières premières du monde extérieur et des frais payés en euros, comment vais-je pouvoir vendre mes produits en vegetas ?**
    *   Si les frais de création du produit et les matières premières sont entièrement achetés en euros, il est facile de constater que vous n'avez d'autres choix que de vendre vos produits entièrement en euros.
    *   Cependant, au fur et à mesure que la communauté grandit et que de plus en plus d'organisations de services et produits sont crées, vous pourrez tendre à convertir le prix de votre produit entièrement en vegetas.
    *   Pour encourager cela, les organisations peuvent exposer leurs dépenses régulièrement à la communauté afin que d'autres membres ou organisations puissent savoir ce qu'il est nécessaire de produire pour que la communauté puisse devenir auto-suffisante.
